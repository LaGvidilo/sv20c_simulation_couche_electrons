#coding: utf-8
# =====================================
#  Author: Rick Sanchez
#  Status of Source: Not finish /
#					 and not tested
# =====================================
# STUDIE IMPLEMENTATION #0001
# =====================================
# ~~~
# ====
# --SV20C UNISTRA--
# ====
# ~~~
# =====================================

# IMPORTATIONS DE DEPENDANCES INTERNE
from math import *
from operator import *

# DEFINITION DES CONSTANTES RELATIVES AUX MODES D'EGUALITES
EQUAL_MODE = 0
PLUS_MODE = 1
LESS_MODE = -1


# =====================================
# ====
# --CLASSE DE TYPE OBJECT--
# ====
#	OPERATEUR SIGMA (SOMMATION)
#	OBJET A USAGE PLUS LARGE
# 	*IMPLEMENTATION DE FONCTION RETOUR
#	 DES VALEURS DEFINIE DE LA RANGE
# =====================================
class sigma_operator_sum(object):
	# =====================================
	#	USAGE: [!! IS AN OBJECT !!]
	#	sigma_operator_sum(k,i,n).compute()
	# =====================================
	def __init__(self,k,i=0,n=inf):
		self.k,self.i,self.n = k,i,n
		
	def compute(self):
		# =====================================
		# Note:
		#	add <, >, = modes option for beg_at
		#	for the fucking sigma OP func
		result = sigma_operator_sum(self.k,self.i,self.n)
		return result
		
	def get_beg_at(self):
		return self.i
		
	def get_end_at(self):
		return self.n
	
	def getter(self):
		return tuple([i,n])
		
	def setter(self,i,n):
		self.i,self.n = i,n
		
	def set_beg_at(self,i):
		self.i = i
		
	def set_end_at(self,n):
		self.n = n
	
	def sigma_operator_sum(k,beg_at=-inf,end_at=inf,mode=EQUAL_MOD):
		#Faire le calcul d'indexation de la pile !!!
		return sum([value for value in k[beg_at:end_at]])
# =====================================
# =====================================	
	
	
	
# =====================================
# ====
# --CLASSE--
# ====
# 	Hamiltonien effectif electronique
# =====================================	
class Hamiltonien_effectif:
	def heff(N,M,Z,r,delta):
		he = (-1/2) * self.sigma_operator_sum(k=self.square_all(delta),beg_at=1,end_at=N)  - self.sigma_operator_sum(k=self.sigma_operator_sum(k=divide_alls(Z,r),beg_at=1,end_at=M),beg_at=1,end_at=N) + self.sigma_operator_sum(k=self.sigma_operator_sum(k=(1/r),beg_at= ,end_at= ),beg_at= ,end_at= )
		# =====================================
		# 	Ceci est faux dans la mesure où
		#	sigma_operator_sum ne retourne pas
		#	de variables internes dont on a 
		#	besoin.
		# 	Donc ma conclusion est que la
		#	fonction sigma_operator_sum doit
		#	être devenir un Object et non
		#	rester une fonction de la classe
		# 	Hamiltonien_effectif.
		# ===================================== 
		
		
	def sigma_operator_sum(k,beg_at=-inf,end_at=inf):
		# =====================================
		# Note:
		#	add <, >, = modes option for beg_at
		return sum([value for value in k[beg_at:end_at]])
		
	def square_all(coll):
		return [x**2 for x in coll]

	def divide_all(coll):
		pass
	
# =====================================
# =====================================