# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = elements_layers_electrons_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Optional, List, Any, TypeVar, Callable, Type, cast


T = TypeVar("T")


def from_none(x: Any) -> Any:
	assert x is None
	return x


def from_str(x: Any) -> str:
	assert isinstance(x, str)
	return x


def from_union(fs, x):
	for f in fs:
		try:
			return f(x)
		except:
			pass
	assert False


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
	assert isinstance(x, list)
	return [f(y) for y in x]


def is_type(t: Type[T], x: Any) -> T:
	assert isinstance(x, t)
	return x


def to_class(c: Type[T], x: Any) -> dict:
	assert isinstance(x, c)
	return cast(Any, x).to_dict()


@dataclass
class ElementsLayersElectron:
	numeros_atomique: Optional[int] = None
	symbole: Optional[str] = None
	élément_chimique: Optional[str] = None
	famille_d_éléments: Optional[str] = None
	électrons_par_couche: Optional[List[str]] = None

	@staticmethod
	def from_dict(obj: Any) -> 'ElementsLayersElectron':
		assert isinstance(obj, dict)
		numeros_atomique = from_union([from_none, lambda x: int(from_str(x))], obj.get("numeros atomique"))
		symbole = from_union([from_str, from_none], obj.get("symbole"))
		élément_chimique = from_union([from_str, from_none], obj.get("Élément chimique"))
		famille_d_éléments = from_union([from_str, from_none], obj.get("Famille d'éléments"))
		électrons_par_couche = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Électrons par couche"))
		return ElementsLayersElectron(numeros_atomique, symbole, élément_chimique, famille_d_éléments, électrons_par_couche)

	def to_dict(self) -> dict:
		result: dict = {}
		result["numeros atomique"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)), lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.numeros_atomique)
		result["symbole"] = from_union([from_str, from_none], self.symbole)
		result["Élément chimique"] = from_union([from_str, from_none], self.élément_chimique)
		result["Famille d'éléments"] = from_union([from_str, from_none], self.famille_d_éléments)
		result["Électrons par couche"] = from_union([lambda x: from_list(from_str, x), from_none], self.électrons_par_couche)
		return result


def elements_layers_electrons_from_dict(s: Any) -> List[ElementsLayersElectron]:
	return from_list(ElementsLayersElectron.from_dict, s)


def elements_layers_electrons_to_dict(x: List[ElementsLayersElectron]) -> Any:
	return from_list(lambda x: to_class(ElementsLayersElectron, x), x)
